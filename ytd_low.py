from pytube import YouTube
from moviepy.editor import *
from shutil import move

final_location = 'C:/Users/Admin/Desktop/DownloadedSongs'


def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__

def main():
    url = input("[?] YT link: ")
    try:
        blockPrint()
        mp4 = YouTube(url).streams.get_highest_resolution().download()
        mp3 = mp4.split(".mp4", 1)[0] + f".mp3"
        video_clip = VideoFileClip(mp4)
        audio_clip = video_clip.audio
        audio_clip.write_audiofile(mp3)

        audio_clip.close()
        video_clip.close()

        os.remove(mp4)
        enablePrint()
        print("[-] Done")
        try:
            move(mp3, final_location)
        except:
            print("[!] edit your path correctly! (maybe already downloaded)")
    
    except:
        print("[!] Wrong link !!")
    print("")
    main()



if __name__ == "__main__":
    main()
